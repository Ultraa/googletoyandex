//
//  ViewController.swift
//  GoogleToYandex
//
//  Created by User Userovich on 19.01.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var infoAboutPlaceTextView: UITextView!
    
    var lat = 45.711547
    var long = 33.684783
    
    var marker: GMSMarker?
    var address: String?
    var isStreetExist: Bool?
    var country: String?
    var coordinates: CLLocationCoordinate2D?
    var urlStringYandex = "https://geocode-maps.yandex.ru/1.x/?format=json&lang=ru_RU&geocode="
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMap.delegate = self
        infoAboutPlaceTextView.text = ""
    }

    func setupController() {
        let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: long)
        latitudeTextField.text = "\(lat)"
        longitudeTextField.text = "\(long)"
        let camera = GMSCameraPosition.camera(withTarget: coordinates, zoom: 10)
        viewMap.camera = camera

        let marker = GMSMarker()
        
        marker.position = camera.target
        marker.snippet = "TestMarker"
        marker.appearAnimation = .pop
        marker.map = viewMap
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        marker = GMSMarker(position: coordinate)
        self.marker?.map = self.viewMap
        
        let geocoder = GMSGeocoder()

        geocoder.reverseGeocodeCoordinate(coordinate, completionHandler: { response, error in
            if response != nil {
                self.address = (response?.firstResult()?.lines?[0])!
                self.marker?.snippet = self.address
                self.infoAboutPlaceTextView.text = self.address
                self.latitudeTextField.text = "\(self.coordinates?.latitude ?? 0.0)"
                self.longitudeTextField.text = "\(self.coordinates?.longitude ?? 0.0)"
                self.coordinates = coordinate
            } else { // Сюда попадаем если адрес не найден (например для любой точки Крыма) и пробуем загрузить информацию из яндекс.карт по этим же координатам
                
                self.yandexReverseGeocodeCoordinate(coordinates: coordinate, completion: { (address, isStreetExist, country, error) in
                    self.address = address
                    self.coordinates = coordinate
                    DispatchQueue.main.async {
                        self.marker?.snippet = self.address
                        if isStreetExist {
                            self.infoAboutPlaceTextView.text = "Это улица!, " + self.address!
                        } else {
                            self.infoAboutPlaceTextView.text = "не улица :(, " + self.address!
                        }
                        self.latitudeTextField.text = "\(self.coordinates?.latitude ?? 0.0)"
                        self.longitudeTextField.text = "\(self.coordinates?.longitude ?? 0.0)"
                    }
                })
            }
            
            self.viewMap.selectedMarker = self.marker
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: mapView.camera.zoom > 10 ? mapView.camera.zoom : 10)
            CATransaction.begin()
            CATransaction.setValue(1.5, forKey: kCATransactionAnimationDuration)
            self.viewMap.animate(to: camera)
            CATransaction.commit()
        })
    }
    
    /// Функция получения адреса и проверки наличия улицы из геокодера Яндекса. На вход принимает CLLocationCoordinate2D, а возвращает address: String, isStreetExist: Bool, country: String
    func yandexReverseGeocodeCoordinate(coordinates: CLLocationCoordinate2D, completion: @escaping (String, Bool, String, Error?) -> Void ) -> Void {
        
        let urlString = urlStringYandex+"\(coordinates.longitude),\(coordinates.latitude)"
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data, error == nil else {
                completion("", false, "", error)
                return
            }
            do {
                let information = try JSONDecoder().decode(YandexData.self, from: data)
                var address = ""
                var isStreetExist = false
                var country = ""
                
                guard let fullAddress = information.response?.GeoObjectCollection?.featureMember[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.text else { return }
                address = fullAddress // Парсим самый подробный адрес
                
                // Парсим наличие у адреса улицы
                if !(information.response?.GeoObjectCollection?.featureMember[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components.filter( {$0?.kind == "street"}).isEmpty)! {
                    isStreetExist = true
                }
                
                // Парсим название страны (если это страна)
                let countryName = information.response?.GeoObjectCollection?.featureMember[0]?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components.filter( {$0?.kind == "country"}) ?? []
                country = !(countryName.isEmpty) ? (countryName[0]?.name)! : ""

                completion(address, isStreetExist, country, nil)
            } catch let errDecode {
                completion("", false, "", errDecode)
            }
        }.resume()
    }
}

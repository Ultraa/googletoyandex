//
//  Place.swift
//  GoogleToYandex
//
//  Created by User Userovich on 30.01.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import Foundation
import MapKit

class Place: NSObject, NSCoding {
    
    var address: String = ""
    var latitude: Double? = nil
    var longitude: Double? = nil
    var streetName: String? = nil
    
    init(address: String, location: CLLocationCoordinate2D, streetName: String?) {
        self.address = address
        self.latitude = location.latitude
        self.longitude = location.longitude
        self.streetName = streetName
    }
    
    required init(coder decoder: NSCoder) {
        self.address = decoder.decodeObject(forKey: "address") as? String ?? ""
        self.latitude = decoder.decodeObject(forKey: "latitude") as? Double ?? nil
        self.longitude = decoder.decodeObject(forKey: "longitude") as? Double ?? nil
        self.streetName = decoder.decodeObject(forKey: "streetName") as? String ?? nil
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(address, forKey: "address")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(streetName, forKey: "streetName")
    }
    
    func save(forKey: String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: forKey)
        UserDefaults.standard.synchronize()
    }
    
    class func clear(forKey: String) {
        if UserDefaults.standard.object(forKey: forKey) != nil{
            UserDefaults.standard.removeObject(forKey: forKey)
        }
    }
    
    class func loadSaved(forKey: String) -> Place? {
        if let data = UserDefaults.standard.object(forKey: forKey) as? Data {
            let userData = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? Place
            return userData
        }
        return nil
    }
}

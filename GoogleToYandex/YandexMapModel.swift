//
//  YandexMapModel.swift
//  GoogleToYandex
//
//  Created by User Userovich on 22.01.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import Foundation

/// JSON-объект вернувшийся после HTTP-запроса с yandex.API
struct YandexData: Decodable {
    var response: Response?
}

struct Response: Decodable {
    var GeoObjectCollection: FeatureMember?
}

struct FeatureMember: Decodable {
    var featureMember: [GeoObject?]
}

struct GeoObject: Decodable {
    var GeoObject: MetaDataProperty?
}

struct MetaDataProperty: Decodable {
    var metaDataProperty: GeocoderMetaData?
}

struct GeocoderMetaData: Decodable {
    var GeocoderMetaData: AddressData?
}

struct AddressData: Decodable {
    /// Тип объекта (дом, или еще что-то, не проверял)
    var kind: String?
    /// Полный подробный адрес (Страна, федеративный округ, город, улица, дом)
    var text: String?
    /// Точность (точно или не точно, не очень понятно что это)
    var precision: String?
    /// Адрес, разбиваемый на части (можно получить только страну, или например только улицу, по индексу массива Address.Components[index]
    var Address: Address?
}

struct Address: Decodable {
    /// Код страны (например RU)
    var countryCode: String?
    /// Почтовый индекс
    var postalCode: String?
    /// Строка адреса
    var formatted: String?
    /// Компоненты: kind и name (например kind: country, name: Россия)
    var Components: [Components?]
}

struct Components: Decodable {
    var kind: String?
    var name: String?
}

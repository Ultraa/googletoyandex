//
//  SearchViewController.swift
//  GoogleToYandex
//
//  Created by User Userovich on 29.01.2018.
//  Copyright © 2018 User Userovich. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SearchViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var searchView: UIView!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var selectedPlace: Place?
    
    var sendTimer = Timer()
    var counterTimer = Int()
    var tempString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
//        searchController?.searchResultsUpdater = resultsViewController // закоментить
        searchView.addSubview((searchController?.searchBar)!)
                                                                     
        searchController?.searchBar.delegate = self // раскоментить
    }
    
    
    // MARK:- Main Logic - раскоментить
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        tempString = searchText
        sendTimer.invalidate()
        counterTimer = 0
        sendTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(printConsole), userInfo: nil, repeats: true)
    }

    @objc func printConsole() {
        if tempString.count > 0 {
            counterTimer += 1
            print("- \(counterTimer) -")
            if counterTimer == 2 {
                sendTimer.invalidate()
                print("Строка поиска: \(tempString)")
                searchController?.searchResultsUpdater = resultsViewController
                searchController?.searchBar.text = tempString
                searchController?.searchResultsUpdater = nil
                counterTimer = 0
                tempString = ""
            } else {
                searchController?.searchResultsUpdater = nil
                print("Request was not sent")
            }
        } else {
            sendTimer.invalidate()
            searchController?.searchResultsUpdater = nil
            print("Search bar is empty")
        }
    }
}

extension SearchViewController: GMSAutocompleteResultsViewControllerDelegate {

    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        var streetName: String?
        for each in place.addressComponents ?? [] where each.type == "route" {
            streetName = each.name
        }
        
        if place.formattedAddress != nil {
            self.selectedPlace = Place(address: place.formattedAddress!, location: place.coordinate, streetName: streetName)
        } else {
            self.selectedPlace = Place(address: place.name, location: place.coordinate, streetName: streetName)
        }
        
        searchController?.searchBar.text = place.name
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print("Error (FilterPlaceViewController): ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
